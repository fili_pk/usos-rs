# usos-rs

[![crates.io](https://img.shields.io/crates/v/usos-rs.svg)](https://crates.io/crates/usos-rs)

The `usos-rs` crate provides a convenient way to authenticate to [USOS api](https://apps.usos.edu.pl)

## Examples

### Getting first name of the user
```rust
use reqwest::Url;
use std::error::Error;
use std::{collections::HashSet, io};
use usos_rs::{self, Scope, UsosRs};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let consumer_key = "consumer key";
    let consumer_secret = "consumer secret";
    let api_url = Url::parse("api url")?;
    let usos = UsosRs::new(api_url, consumer_key.to_owned(), consumer_secret.to_owned())?;

    let scopes = HashSet::from([Scope::PersonalData]);
    let u_user = usos.get_unauthorized_user(Some(&scopes)).await?;

    println!("{}", u_user.authorize_url());
    let mut user_input = String::new();
    io::stdin().read_line(&mut user_input)?;
    let pin = user_input.trim();
    let user = u_user.authorize(pin).await.map_err(|(_, e)| e)?;

    let response = user
        .reqwest_client()
        .post(user.api_url().join("/services/users/user")?)
        .query(&[("fields", "first_name")])
        .send()
        .await?;

    println!("{}", response.text().await?);

    Ok(())
}
```

### Getting first name of already authorized user
```rust
use reqwest::Url;
use std::error::Error;
use usos_rs::{self, AccessToken, UsosRs};

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let consumer_key = "consumer key";
    let consumer_secret = "consumer secret";
    let api_url = Url::parse("api url")?;
    let usos = UsosRs::new(api_url, consumer_key.to_owned(), consumer_secret.to_owned())?;

    let user = usos
        .get_authorized_user(AccessToken {
            token: "access token".to_owned(),
            token_secret: "access token secret".to_owned(),
        })
        .await;

    let response = user
        .reqwest_client()
        .post(user.api_url().join("/services/users/user")?)
        .query(&[("fields", "first_name")])
        .send()
        .await?;

    println!("{}", response.text().await?);

    Ok(())
}
```

## License
[AGPLv3+](LICENSE) © Filip K.